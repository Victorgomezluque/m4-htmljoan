= Qüestions d'introducció a l'HTML
:doctype: article
:encoding: utf-8
:lang: ca
:numbered:
:ascii-ids:

== Documentació

link:http://www.w3schools.com/html/html_intro.asp[W3Schools: Introduction to HTML]
link:http://www.w3schools.com/html/html_xhtml.asp[W3Schools: HTML / XHTML]
link:http://www.w3schools.com/html/html5_intro.asp[W3Schools: HTML5]
link:https://whatwg.org/[https://whatwg.org/]

== Qüestions

1. Què és i per a què s'utilitza l'HTML?

2. HTML és l'acrònim de _Hypertext Markup Language_. Què vol dir _Hypertext_?

3. Què és l'XHTML i quina relació té amb l'XML?

4. Quins problemes de l'HTML es van resoldre amb l'XHTML?

5. Què és l'HTML5? Per a què es va crear?

6. Què significa WHATWG? Quin és l'objectiu del WHATWG?

7. Cerca alguna característica de l'HTML5 que s'hagi incorporat els últims dos
anys.
